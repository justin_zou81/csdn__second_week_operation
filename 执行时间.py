import time

def metric(fn):	# 设计装饰器
    # 对程序进行封装
    def inner():
        # 在程序执行之前记录一次时间
        start_time = time.time()
        # 需要执行一次程序  
        fn()               
        # 在程序执行之后记录一次时间
        end_time = time.time()
        # 两次时间相见，得到的差就是程序执行时间
        print('耗时：{:.8f}s'.format(end_time - start_time))    
    return inner

@metric
def multiplication_table():
	# 利用生成九九乘法表进行测试
    print('正常右上角写法的while思路')
    row = 9
    while row >= 1:
        col = row
        n = 0
        while n < 9 - row:
            print(' ' * 9, end='')
            n += 1
        while col >=1:
            print('{}*{}={:<5d}'.format(row, col, row * col),end='')
            col -= 1
        print('')
        row -= 1

multiplication_table()